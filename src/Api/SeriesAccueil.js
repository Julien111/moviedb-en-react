import React, { useState, useEffect } from 'react';
import "./Serie.css";
import { key } from '../redux/apiKey';
import { Link } from "react-router-dom";
import SearchTv from "../Components/SearchTv/SearchTv";

export default function SeriesAccueil() {

    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [items, setItems] = useState([]);
    const images = "https://image.tmdb.org/t/p/w500/";


    useEffect(() => {
        for (let i = 1; i < 3; i++) {
            fetch(`https://api.themoviedb.org/3/tv/popular?api_key=${key}&language=en-US&page=1`).then(response => response.json()).then(data => {
                setIsLoaded(true);
                setItems(data.results);
            },
                (error) => {
                    setIsLoaded(true);
                    setError(error);
                }
            )
        }
    }, []);

    if (error) {
        return <div>Erreur : {error.message}</div>;
    } else if (!isLoaded) {
        return <div>Chargement...</div>;
    } else {

        return (
            <div className="container">
                <h2 className="text-center my-1">Best TV series</h2>
                <SearchTv />
                <div className="row row-cols-2 row-cols-lg-5 g-2 g-lg-3 my-2 justify-content-center">

                    {items.map((serie) =>
                        <div className="col card m-1 shadow p-3 mb-3 bg-white" width="18rem" key={serie.id}>

                            <img className="card-img-top imgCard" height="250px" src={images + serie.poster_path} alt={serie.title} />
                            <div className="card-body">
                                <h5 className="card-title">{serie.name}</h5>
                                <p><span className="average">Average grade:</span> {serie.vote_average}</p>
                            </div>
                            <div className="text-center">
                                <Link className="btn btn-primary my-1" to={{
                                    pathname: `series/${serie.name.replace(/\s+/g, '-').trim()}`,
                                    state: {
                                        id: serie.id,
                                        name: serie.name,
                                        note: serie.vote_average,
                                        image: images,
                                        poster: serie.poster_path,
                                        resume: serie.overview,
                                    },
                                }}>
                                    More details
                                </Link>
                            </div>

                        </div>
                    )}

                </div>
            </div>
        );
    }

}