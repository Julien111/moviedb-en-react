import React, { useState } from 'react';
import { useHistory } from "react-router-dom";
import "./SearchTv.css";

export default function SearchTv() {


    const [terms, setTerms] = useState("");
    const history = useHistory();


    const handleChange = (props) => {
        setTerms(props);
    };


    const handleSubmit = (e) => {
        e.preventDefault();
        let params = terms.replace(/:/g, '').trim();
        let serieStr = params.replace(/\s+/g, '+').trim();
        history.push("/search/series/" + serieStr, { name: serieStr });
    };



    return (
        <div>
            <div className="form-movie">
                <form onSubmit={handleSubmit}>
                    <div className="form-elt">
                        <div className="elt">
                            <input type="text" name="movie" value={terms} className="movie-input" placeholder="Search Tv series" onChange={(e) => handleChange(e.target.value)} />
                        </div>
                        <div className="elt">
                            <input type="submit" className="btn btn-primary" value="Search" />
                        </div>
                    </div>
                </form>
            </div>
        </div>

    );

}
