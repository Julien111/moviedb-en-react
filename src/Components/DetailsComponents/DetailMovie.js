import React, { useState, useEffect } from 'react';
import { useLocation } from "react-router-dom";
import "./Detail.css";
import { key } from "../../redux/apiKey";

export default function DetailMovie() {

    const lienYouTube = "https://www.youtube-nocookie.com/embed/";
    const location = useLocation();
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [items, setItems] = useState([]);
    const movie_id = location.state.id;

    useEffect(() => {

        fetch(`https://api.themoviedb.org/3/movie/${movie_id}/videos?api_key=${key}&language=en-US`).then(response => response.json()).then(data => {
            setIsLoaded(true);
            setItems(data.results[0]);
        },
            (error) => {
                setIsLoaded(true);
                setError(error);
            }
        )
        //eslint-disable-next-line
    }, []);
    if (error) {
        return <div>Erreur : {error.message}</div>;
    } else if (!isLoaded) {
        return <div>Chargement...</div>;
    } else {

        return (
            <div className="element_detail" key={location.state.id}>
                <div className="cardElement">
                    <div className="img_element">
                        <img className="img_element" height="250px" src={location.state.image + location.state.poster} alt={location.state.name} />
                    </div>
                    <h2 className="title">{location.state.name}</h2>
                    <div className="body_element">
                        <p className="resume"><span className="resume_element">Resume :</span> {location.state.resume}</p>
                        <p><span className="average_element">Average grade :</span> {location.state.note}</p>
                    </div>
                    <h4 className="video">Trailer</h4>
                    <div className="embed-responsive embed-responsive-4by3 video">
                        <iframe title="trailer video" width="360" height="250" className="embed-responsive-item" src={lienYouTube + items.key} allowFullScreen></iframe>
                    </div>
                </div>
            </div>
        )
    }
}
