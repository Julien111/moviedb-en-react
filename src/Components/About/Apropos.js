import React from 'react';
import "./Apropos.css";

export default function Apropos() {
    return (
        <div className="about_element">
            <div className="aboutContent">
                <h2 className="title_element">About us</h2>
                <p className="body_element">We try to use the movie DB API with React.js. The purpose of the project is to progress in JavaScript and in the use of framework React.</p>
            </div>

        </div>
    )
}
