import { useHistory } from "react-router-dom";
import React, { useState, useEffect } from 'react';
import { key } from '../../redux/apiKey';
import { Link } from "react-router-dom";

export default function SearchPart() {

    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [items, setItems] = useState([]);
    const images = "https://image.tmdb.org/t/p/w500/";

    const history = useHistory();
    let movieName = history.location.state.name;


    useEffect(() => {

        fetch(`https://api.themoviedb.org/3/search/movie?api_key=${key}&query=${movieName}`).then(response => response.json()).then(data => {
            setIsLoaded(true);
            setItems(data.results);
        },
            (error) => {
                setIsLoaded(true);
                setError(error);
            }
        )
        //eslint-disable-next-line
    }, []);

    if (error) {
        return <div>Erreur : {error.message}</div>;
    } else if (!isLoaded) {
        return <div>Chargement...</div>;
    } else {

        return (
            <div className="container">
                <h2 className="text-center my-1">Results for your movies</h2>
                <div className="row row-cols-2 row-cols-lg-5 g-2 g-lg-3 my-2 justify-content-center">

                    {items.map((movie) =>
                        <div className="col card m-1 shadow p-3 mb-3 bg-white" width="18rem" key={movie.id}>

                            <img className="card-img-top imgCard" height="250px" src={images + movie.poster_path} alt={movie.title} />
                            <div className="card-body">
                                <h5 className="card-title">{movie.title}</h5>
                                <p><span className="average">Average grade:</span> {movie.vote_average}</p>
                            </div>
                            <div className="text-center">
                                <Link className="btn btn-primary my-1" to={{
                                    pathname: `/movies/${movie.title.replace(/\s+/g, '-').trim()}`,
                                    state: {
                                        id: movie.id,
                                        name: movie.title,
                                        note: movie.vote_average,
                                        image: images,
                                        poster: movie.poster_path,
                                        resume: movie.overview,
                                    },
                                }}>
                                    More details
                                </Link>
                            </div>

                        </div>
                    )}

                </div>
            </div>
        );
    }

}
