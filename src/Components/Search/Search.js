import React, { useState } from 'react';
import { useHistory } from "react-router-dom";
import "./Search.css";

export default function Search() {


    const [terms, setTerms] = useState("");
    const history = useHistory();


    const handleChange = (props) => {
        setTerms(props);
    };


    const handleSubmit = (e) => {
        e.preventDefault();
        let params = terms.replace(/:/g, '').trim();
        let movieStr = params.replace(/\s+/g, '+').trim();
        history.push("/search/" + movieStr, { name: movieStr });
    };



    return (
        <div className="container">
            <h1 className="text-center my-1">The movie DB</h1>
            <div className="form-movie">
                <form onSubmit={handleSubmit}>
                    <div className="form-elt">
                        <div className="elt">
                            <input type="text" name="movie" value={terms} className="movie-input" placeholder="Search movies" onChange={(e) => handleChange(e.target.value)} />
                        </div>
                        <div className="elt">
                            <input type="submit" className="btn btn-primary" value="Search" />
                        </div>
                    </div>
                </form>
            </div>
        </div>

    );

}
