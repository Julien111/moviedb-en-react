import React from 'react'
import MoviesAccueil from "../Api/MoviesAccueil";
import SeriesAccueil from "../Api/SeriesAccueil";
import Search from "./Search/Search";


export default function Accueil() {
    return (
        <div>
            <Search />
            <MoviesAccueil />
            <SeriesAccueil />
        </div>
    )
}

