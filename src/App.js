import './App.css';
import Accueil from "./Components/Accueil";
import Movies from "./Components/Movies/Movies";
import Series from "./Components/Series/Series";
import Apropos from "./Components/About/Apropos";
import DetailMovie from "./Components/DetailsComponents/DetailMovie";
import DetailTv from "./Components/DetailsComponents/DetailTv";
import Nav from "./Components/Partials/Navbar";
import SearchPart from "./Components/SearchPart/SearchPart";
import SearchSeriePart from "./Components/SearchSeriePart/SearchSeriePart";
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

function App() {
  return (
    <>
      <Router>
        <Nav />
        <Switch>
          <Route path="/" exact component={Accueil} />
          <Route path="/movies" exact component={Movies} />
          <Route path="/movies/:slug" exact component={DetailMovie} />
          <Route path="/series" exact component={Series} />
          <Route path="/series/:slug" exact component={DetailTv} />
          <Route path="/apropos" exact component={Apropos} />
          <Route path="/search/:slug" exact component={SearchPart} />
          <Route path="/search/series/:slug" exact component={SearchSeriePart} />
        </Switch>
      </Router>
    </>
  );
}

export default App;
